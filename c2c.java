package mods.c2c.common;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.network.NetworkMod;
import cpw.mods.fml.common.registry.GameRegistry;

@Mod(modid = "jonessc_c2c", name = "Coal 2 Charcoal", version = "2.1.0.0")
@NetworkMod(clientSideRequired = true, serverSideRequired = false)
public class c2c
{
    @EventHandler
    public static void init(FMLInitializationEvent event)
    {
        GameRegistry.addShapelessRecipe(new ItemStack(Item.coal, 1, 0), new ItemStack(Item.coal, 1, 1));
        GameRegistry.addShapelessRecipe(new ItemStack(Item.coal, 1, 1), new ItemStack(Item.coal, 1, 0));
    }
}
